import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import Toast, { BaseToast }  from 'react-native-toast-message';

import authReducer from './src/components/Auth/store/reducer';
import appUtilitiesReducer from './src/store/reducer';
import dashboardReducer from './src/components/Dashboard/store/reducer';

import { COLORS } from './constants';

import Cashlet from './src/Cashlet';

const middlewares = [thunk];
const reducer = combineReducers({
    auth: authReducer,
    utilities: appUtilitiesReducer,
    dashboard: dashboardReducer
});

const logger = store => next => action => {
    console.log('[Middleware] Dispatching', action);
    const result = next(action);
    console.log('[Middleware] next state', store.getState());
    return result;
};

if(process.env.NODE_ENV === 'development') middlewares.push(logger);

const store = createStore(reducer, applyMiddleware(...middlewares));

const snackbarConfig = {
    success: ({ text1, text2, props, ...rest }) => (
        <BaseToast
            {...rest}
            style={{ borderLeftColor: COLORS.RED, backgroundColor: COLORS.RED }}
            contentContainerStyle={{ paddingHorizontal: 15 }}
            text1={text1}
            text2={text2}
            text2Style={{
                fontSize: 15,
                fontWeight: '400',
                color: '#fff',
                fontFamily: 'Comfortaa_300Light'
            }}
            onTrailingIconPress={() => Toast.hide()}
        />
    )
};

export default function App() {
    return (
      <Provider store={store}>
          <View style={styles.container}>
              <Cashlet />
              <StatusBar style="auto" />
          </View>
          <Toast config={snackbarConfig} ref={ref => Toast.setRef(ref)} />
      </Provider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
