import React from 'react';

export default auth = React.createContext({
    userName: null,
    userCurrency: null,
    availableFunds: null
});