const currentState = {
    loading: false,
    snackbarOpen: false,
    snackbarMessage: null
};

export default reducer = (state = currentState, action) => {
    const { type, payload } = action;

    switch(type) {
        case 'TOGGLE_LOADING':
            const { loading } = state;
            return {
                ...state,
                loading: !loading
            };
        case 'INIT_SNACKBAR': 
            return {
                ...state,
                snackbarOpen: true,
                snackbarMessage: payload
            };
        case 'TOGGLE_SNACKBAR': 
            return {
                ...state, 
                snackbarOpen: false,
                snackbarMessage: null
            };
        case 'SET_SNACKBAR_MESSAGE': 
            return {
                ...state,
                snackbarMessage: payload
            };
        default: 
            return state;
    }
};