import { TOGGLE_LOADING, INIT_SNACKBAR, TOGGLE_SNACKBAR, SET_SNACKBAR_MESSAGE } from '../actionTypes';

export const toggleLoading = () => {
    return {
        type: TOGGLE_LOADING
    };
};

export const initSnackbar = message => {
    return {
        type: INIT_SNACKBAR,
        payload: message
    };
};

export const toggleSnackbar = () => {
    return {
        type: TOGGLE_SNACKBAR
    };
};

export const setSnackbarMessage = message => {
    return {
        type: SET_SNACKBAR_MESSAGE,
        payload: message
    };
};