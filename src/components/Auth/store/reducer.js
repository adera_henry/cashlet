const currentState = {
    userName: null,
    userCurrency: null,
    availableFunds: null
};

export default reducer = (state = currentState, action) => {
    const { type, payload } = action;

    switch(type) {
        case 'INIT_BASE_INFO': 
            const { name, currency, funds } = payload;
            return {
                ...state,
                userName: name,
                userCurrency: currency,
                availableFunds: funds
            }
        default: 
            return state;
    }
};