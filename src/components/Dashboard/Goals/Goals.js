import React, { useContext } from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import ShadowView from 'react-native-shadow-view';
import { Entypo } from '@expo/vector-icons';

import context from '../../../context/auth';
import { COLORS } from '../../../../constants';
import { toggleLoading, initSnackbar } from '../../../store/actions';
import Preloader from './Preloader/Preloader';

const Goals = ({ goals, onToggleLoading, onInitSnackbar }) => {
    const { loading, values, error } = goals;
    const { userCurrency } = useContext(context);

    const buttonPressHandler = () => {
        onToggleLoading();

        return setTimeout(() => {
            onToggleLoading();
            onInitSnackbar('This is a Cashlet in-app notification snackbar.');
        }, 1000);
    };

    return (
        <View style={styles.goalsContainer}>
            <Text style={styles.paragraph}>Your Goals</Text>
            <ScrollView>
                {!loading ? !error ? values ? values.map(goal => {
                    return (
                        <ShadowView key={goal.id} style={styles.goalContainer}>
                            <View style={styles.goalDetailsContainer}>
                                <Text style={styles.goalName}>{goal.name}</Text>
                                <Text style={styles.goalTarget}>{userCurrency + ' ' + goal.target}</Text>
                            </View>
                            <TouchableOpacity style={styles.finishGoalContainer} onPress={() => null}>
                                <Text style={styles.finishGoalText}>Finish Goal</Text>
                            </TouchableOpacity>
                            <View style={styles.chevronContainer}>
                                <Entypo name="chevron-small-right" size={24} color={COLORS.GREY} style={styles.chevron} />
                            </View>
                        </ShadowView>
                    );
                }) : <Text style={styles.noGoal}>No goal set</Text> : <Text style={styles.error}>An error occured</Text> : <Preloader />}
            </ScrollView>
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.button} onPress={buttonPressHandler}>
                    <Text style={styles.buttonText}>Show Snackbar</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    goalsContainer: {
        backgroundColor: '#fff',
        flex: 1,
        width: '100%',
        marginTop: 25,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    },
    goalContainer: {
        marginTop: 10,
        marginBottom: 10,
        width: 340,
        height: 110,
        backgroundColor: '#fff',
        borderRadius: 12,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    finishGoalContainer:{
        backgroundColor: COLORS.GREEN, 
        borderRadius: 20, 
        height: 30, 
        width: 100, 
        marginTop: 40, 
        marginRight: 10
    }, 
    finishGoalText: {
        alignSelf: 'center', 
        color: '#fff', 
        marginTop: 2,
        fontFamily: 'Comfortaa_300Light'
    }, 
    buttonContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        marginLeft: 10,
        marginRight: 10
    },  
    button: {
        position: 'relative',
        bottom: 0,
        borderRadius: 20,
        marginTop: 20,
        marginBottom: 20,
        backgroundColor: COLORS.GREEN,
        height: 50,
        alignItems: 'center'
    },
    buttonText: {
        color: '#fff',
        fontFamily: 'Comfortaa_300Light',
        marginHorizontal: 5,
        marginVertical: 5,
        fontSize: 20
    },  
    paragraph: {
        marginRight: 20,
        marginLeft: 20,
        fontSize: 22,
        color: COLORS.GREY,
        fontFamily: 'Comfortaa_700Bold'
    },
    goalDetailsContainer: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 25
    },
    goalName: {
        fontSize: 20,
        marginLeft: 10,
        color: 'grey',
        fontFamily: 'Comfortaa_600SemiBold'
    },
    goalTarget: {
        fontSize: 15,
        marginLeft: 10,
        color: '#ccc',
        fontFamily: 'Comfortaa_300Light'
    },
    chevronContainer: {
        borderLeftColor: '#ccc', 
        borderLeftWidth: 1
    },
    chevron: {
        marginTop: 45, 
        marginRight: 20, 
        marginLeft: 20
    },
    noGoal: {
        color: COLORS.RED,
        fontFamily: 'Comfortaa_300Light'
    },
    error: {
        color: COLORS.RED,
        fontSize: 20,
        fontFamily: 'Comfortaa_300Light'
    }
});

const mapStateToProps = state => {
    return {
        goals: state.dashboard.goals,
        utils: state.utilities
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onToggleLoading: () => dispatch(toggleLoading()),
        onInitSnackbar: message => dispatch(initSnackbar(message))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Goals)