import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';

import { COLORS } from '../../../../../constants';

const preloader = () => (
    <View style={styles.container}>
        <ActivityIndicator size='small' color={COLORS.GREEN} />
    </View>
);

const styles = StyleSheet.create({
    container: {
        marginTop: '40%'
    }
});

export default preloader;