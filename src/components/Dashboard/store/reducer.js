const currentState = {
    goals: {
        loading: true,
        values: null,
        error: false
    }
};

export default reducer = (state = currentState, action) => {
    const { type, payload } = action;

    switch(type) {
        case 'INIT_GOALS': {
            const goals = {...state.goals};

            goals.loading = false;
            goals.values = payload;

            return {
                ...state, 
                goals: goals
            };
        }
        case 'SET_GOALS_ERROR': {
            const goals = {...state.goals};

            goals.loading = false;
            goals.error = true;

            return {
                ...state, 
                goals: goals
            };
        } 
        case 'FINISH_GOAL': {
            const goals = {...state.goals};
            const { values } = goals;
            const newValues = Object.assign([], values);
            const index = newValues.findIndex(goal => parseInt(goal.id) === parseInt(payload.id));

            if(index >= 0) {
                const goal = newValues[index];
                goal.complete = true;

                newValues[index] = goal;
                goals.values = newValues;

                return {
                    ...state,
                    goals: goals
                };
            }
        }
        case 'RESET_GOALS': {
            const goals = {...state.goals};

            goals.loading = true;
            goals.values = null;
            goals.error = false;

            return {
                ...state, 
                goals: goals
            };
        } 
        default: 
            return state;
    }
};