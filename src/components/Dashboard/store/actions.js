import { INIT_GOALS, FINISH_GOAL, RESET_GOALS } from '../../../actionTypes';

const dummyGoals = [
    {
        id: 1,
        name: 'Goal 1',
        target: '12,000',
        complete: false
    },
    {
        id: 2,
        name: 'Goal 2',
        target: '12,000',
        complete: false
    }
];

export const initGoals = () => {
    return dispatch => setTimeout(() => dispatch((() => {
        return {
            type: INIT_GOALS,
            payload: dummyGoals
        };
    })()), 5000);
};

export const finishGoal = id => {
    return {
        type: FINISH_GOAL,
        payload: {
            id: id
        }
    };
};

export const resetGoals = () => {
    return {
        type: RESET_GOALS
    };
};