import React, { useEffect, useContext } from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { useFonts, Comfortaa_300Light, Comfortaa_600SemiBold, Comfortaa_700Bold } from "@expo-google-fonts/dev/index";

import { COLORS } from '../../../constants';
import { initGoals, resetGoals } from './store/actions';
import { initBaseInfo } from '../Auth/store/actions';
import Goals from './Goals/Goals';

import context from '../../context/auth';

const Dashboard = ({ onInitBaseInfo, onInitGoals, onResetGoals }) => {
    const [fontLoaded] = useFonts({Comfortaa_300Light, Comfortaa_600SemiBold, Comfortaa_700Bold});
    const { userName, userCurrency, availableFunds} = useContext(context);

    useEffect(() => {
        onInitBaseInfo();
        onInitGoals();

        return () => onResetGoals();
    }, []);

    if(!fontLoaded || !userName || !userCurrency || !availableFunds) return <ActivityIndicator size='large' color={COLORS.GREEN} />

    return (
        <View style={styles.dashboardContainer}>
            <View style={styles.salutationContainer}>
                <Text style={styles.salutation}>
                    Afternoon, <Text style={styles.userName}>{userName}</Text>
                </Text>
                <Text style={styles.paragraph}>Here's the latest:</Text>
            </View>
            <View style={styles.availableFundsContainer}>
                <Text style={styles.availableFunds}>{userCurrency + ' ' + availableFunds}</Text>
                <Text style={styles.paragraph}>Total funds</Text>
            </View>
            <Goals />
        </View>
    );
};

const styles = StyleSheet.create({
    dashboardContainer: {
        backgroundColor: COLORS.GREY,
        flex: 1,
        width: '100%',
        marginTop: 25
    },
    salutationContainer: {
        marginTop: 20
    },
    salutation: {
        color: '#fff',
        fontSize: 30,
        marginRight: 20,
        marginLeft: 20,
        fontFamily: 'Comfortaa_300Light'
    },
    userName: {
        fontWeight: '700',
        fontFamily: 'Comfortaa_300Light'
    },
    paragraph: {
        marginRight: 20,
        marginLeft: 20,
        fontSize: 20,
        color: '#fff',
        fontFamily: 'Comfortaa_300Light'
    },
    availableFundsContainer: {
        marginTop: 10
    },
    availableFunds: {
        color: COLORS.GREEN,
        fontWeight: '700',
        fontSize: 35,
        marginRight: 20,
        marginLeft: 20,
        fontFamily: 'Comfortaa_300Light'
    }
});

const mapDispatchToProps = dispatch => {
    return {
        onInitBaseInfo: () => dispatch(initBaseInfo()),
        onInitGoals: () => dispatch(initGoals()),
        onResetGoals: () => dispatch(resetGoals())
    };
};

export default connect(null, mapDispatchToProps)(Dashboard);