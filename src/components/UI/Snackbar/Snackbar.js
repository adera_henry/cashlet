import { useEffect } from 'react';
import Toast from 'react-native-toast-message';
import { connect } from 'react-redux';

import { setSnackbarMessage, toggleSnackbar } from '../../../store/actions';

const Snackbar = ({ utils, onSnackbarPress, onToggleSnackbar }) => {
    const { snackbarOpen, snackbarMessage } = utils;

    useEffect(() => {
        if(snackbarOpen) Toast.show({
            type: 'success',
            text1: '',
            text2: snackbarMessage,
            visibilityTime: 10000,
            onHide: () => onToggleSnackbar(),
            position: 'top',
            onPress: () => onSnackbarPress('Cashlet task solution by Henry Adera'),
            autoHide: true
        });
    }, [snackbarOpen, snackbarMessage]);

    return null;
};

const mapStateToProps = state => {
    return {
        utils: state.utilities
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSnackbarPress: message => dispatch(setSnackbarMessage(message)),
        onToggleSnackbar: () => dispatch(toggleSnackbar())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Snackbar);