import React from 'react';
import { connect } from 'react-redux';
import LoadingSpinner from 'react-native-loading-spinner-overlay';

import Dashboard from './components/Dashboard/Dashboard';
import AuthContext from './context/auth';
import Snackbar from './components/UI/Snackbar/Snackbar';
import { COLORS } from '../constants';

const Cashlet = ({ auth, utils }) => (
    <>
        <AuthContext.Provider value={{userName: auth.userName, userCurrency: auth.userCurrency, availableFunds: auth.availableFunds}}>
            <Dashboard />
        </AuthContext.Provider>
        <LoadingSpinner visible={utils.loading} cancelable={true} animation='fade' indicatorStyle={{color: COLORS.GREEN}} />
        <Snackbar />
    </>
);

const mapStateToProps = state => {
    return {
        auth: state.auth,
        utils: state.utilities
    };
};

export default connect(mapStateToProps)(Cashlet);