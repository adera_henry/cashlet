// Application constants

export const COLORS = {
    GREY: '#4B5E7F',
    RED: '#E72180',
    GREEN: '#01E576'
};